package ch.scs.fpgastw.standardlibrary;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;

public class Element {

    @XmlAttribute
    public String type;

    @XmlElement(name = "wire")
    public List<String> wires;

    @XmlElement
    public String instantiation;

    @XmlElement
    public String state;
}

