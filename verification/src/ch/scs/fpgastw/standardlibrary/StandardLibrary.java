package ch.scs.fpgastw.standardlibrary;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "standard_library")
public class StandardLibrary {

    @XmlAttribute
    public String version;

    @XmlElement(name = "element")
    public List<Element> elements;
}

