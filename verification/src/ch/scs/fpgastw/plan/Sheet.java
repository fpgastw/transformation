package ch.scs.fpgastw.plan;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;

public class Sheet {

    @XmlAttribute
    public String name;

    @XmlAttribute
    public float x;

    @XmlAttribute
    public float y;

    @XmlElement(name = "symbol")
    public List<Symbol> symbols;

    @XmlElement(name = "connection")
    public List<Connection> connections;
}
