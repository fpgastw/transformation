package ch.scs.fpgastw.plan;

import javax.xml.bind.annotation.XmlAttribute;

public class Connection {

    @XmlAttribute
    public String uid;

    @XmlAttribute
    public String points;
}
