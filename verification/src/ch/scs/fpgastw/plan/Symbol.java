package ch.scs.fpgastw.plan;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;

public class Symbol {

    @XmlAttribute
    public String type;

    @XmlAttribute
    public float x;

    @XmlAttribute
    public float y;

    @XmlAttribute
    public float rotation;

    @XmlElement(name = "parameter")
    public List<Parameter> parameters;

    @XmlElement(name = "port")
    public List<Port> ports;
}

