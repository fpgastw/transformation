package ch.scs.fpgastw.plan;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "plan")
public class Plan {

    @XmlAttribute
    public String version;

    @XmlElement(name = "sheet")
    public List<Sheet> sheets;

}
