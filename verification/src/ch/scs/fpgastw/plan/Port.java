package ch.scs.fpgastw.plan;

import javax.xml.bind.annotation.XmlAttribute;

public class Port {

    @XmlAttribute
    public String name;

    @XmlAttribute
    public String connection;
}
