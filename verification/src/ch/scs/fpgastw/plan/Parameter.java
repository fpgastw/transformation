package ch.scs.fpgastw.plan;

import javax.xml.bind.annotation.XmlAttribute;

public class Parameter {

    @XmlAttribute
    public String key;

    @XmlAttribute
    public String value;
}
