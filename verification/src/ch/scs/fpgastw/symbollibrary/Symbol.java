package ch.scs.fpgastw.symbollibrary;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;

public class Symbol {

    @XmlAttribute
    public String type;

    @XmlElement
    public Image image;

    @XmlElement(name = "port")
    public List<Port> ports;

    @XmlElement(name = "parameter")
    public List<Parameter> parameters;

    @XmlElement
    public Vhdl vhdl;
}

