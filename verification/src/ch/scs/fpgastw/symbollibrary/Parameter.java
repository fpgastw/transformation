package ch.scs.fpgastw.symbollibrary;

import javax.xml.bind.annotation.XmlAttribute;

public class Parameter {

    @XmlAttribute
    public String name;

    @XmlAttribute
    public int x;

    @XmlAttribute
    public int y;
}

