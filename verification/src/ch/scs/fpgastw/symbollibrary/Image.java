package ch.scs.fpgastw.symbollibrary;

import javax.xml.bind.annotation.XmlAttribute;

public class Image {

    @XmlAttribute
    public String file;

    @XmlAttribute
    public int width;

    @XmlAttribute
    public int height;
}
