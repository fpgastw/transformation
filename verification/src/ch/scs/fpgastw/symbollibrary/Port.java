package ch.scs.fpgastw.symbollibrary;

import javax.xml.bind.annotation.XmlAttribute;

public class Port {

    @XmlAttribute
    public String name;

    @XmlAttribute
    public int x;

    @XmlAttribute
    public int y;
}
