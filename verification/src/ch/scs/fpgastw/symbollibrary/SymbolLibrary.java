package ch.scs.fpgastw.symbollibrary;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "symbol_library")
public class SymbolLibrary {

    @XmlAttribute
    public String version;

    @XmlElement(name = "symbol")
    public List<Symbol> symbols;
}
